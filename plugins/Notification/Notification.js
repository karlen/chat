import  React from 'react';
import  ReactDOM from 'react-dom';
import  Dialog from 'material-ui/lib/dialog';
import  RaisedButton from 'material-ui/lib/raised-button';
import  FlatButton from 'material-ui/lib/flat-button';
import * as color from 'material-ui/lib/styles/colors';

class Notification extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            open: props.open,
            onClose: props.onClose,
            title: props.title,
            label: props.label,
            message: props.children
        };
    }

    handleOpen(){
        this.setState({
            open: true
        });
    }

    handleClose() {
        this.setState({
            open: false
        });
        this.state.onClose();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            open: nextProps.open,
            onClose: nextProps.onClose,
            label: nextProps.label,
            title: nextProps.title,
            message: nextProps.children
        });
    }

    render() {
        return (
            <Dialog
                title={this.state.title}
                actions={[
                          <FlatButton
                            label={this.state.label}
                            primary={true}
                            keyboardFocused={true}
                            onTouchTap={this.handleClose.bind(this)}
                            labelStyle={{color:color.red700}}
                          />,
                        ]}
                modal={false}
                open={this.state.open}
                onRequestClose={this.handleClose.bind(this)}
            >
                {this.state.message}
            </Dialog>
        );
    }

}

export default Notification;