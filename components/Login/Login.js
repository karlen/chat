import  React from 'react';
import  ReactDOM from 'react-dom';
import  styles from './Login.scss';
import * as color from 'material-ui/lib/styles/colors';
import  TextField from 'material-ui/lib/text-field';
import  RaisedButton from 'material-ui/lib/raised-button';
import  CircularProgress from 'material-ui/lib/circular-progress';
import  Notifications from 'plugins/Notification';

styles.label = {
    color: 'white'
};

styles.inputStyle = {
    color: 'white'
};

styles.underlineFocusStyle = {
    borderColor: color.red700
};

styles.submit = {
    backgroundColor : color.red700,
    labelColor: 'white',
    marginTop: 15,
    float: 'right',
    clear:'both'
};


class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dialog: {
                open: false,
                label: 'Закрыть',
                onClose:function(){

                },
                title:'',
                message: ''
            },
            progress: {
                display: 'none'
            }
        };
    }

    showProgress(){
        this.setState(function(previousState){
            var state = previousState;
            state.progress = {
                display: 'block'
            };
            return state;
        });
    }

    hideProgress(){
        this.setState(function(previousState){
            var state = previousState;
            state.progress = {
                display: 'none'
            };
            return state;
        });
    }

    hasErrors() {
        var login = document.getElementById('login').value;
        var password = document.getElementById('password').value;

        var is_mail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(login);
        var is_password = (password.length > 5);

        if(!is_mail || !is_password){
            return {
                title: 'Некорректые данные.',
                message: 'Пожалуйста заполните необходимые поля.'
            }
        }else {
            return false;
        }
    }

    showErrors(errors){
        var _self = this;
        this.setState(function(previousState, currentProps){
            var state = previousState;

            state.dialog.open = true;
            state.dialog.title = errors.title;
            state.dialog.message = errors.message;
            state.dialog.onClose = function(){

                _self.setState(function(previousState) {
                    var state = previousState;
                    state.dialog.open = false;
                    _self.hideProgress();
                    return state;
                });

            };

            return state;
        });
        this.showProgress();
    }

    checkUserData() {
        var _self = this;
        this.setState(function(previousState, currentProps){
            var state = previousState;
            state.disabled = true;
            return state;
        });
        var login = document.getElementById('login').value;
        var password = document.getElementById('password').value;
        this.showProgress();
    }

    submit() {
        var errors = this.hasErrors();
        if(typeof errors === 'object'){
            this.showErrors(errors);
        }else if(errors == false){
            this.checkUserData();
        }
    }

    render() {
        return (
            <div className={styles.main_block}>

                <Notifications
                    open={this.state.dialog.open}
                    onClose={this.state.dialog.onClose}
                    title={this.state.dialog.title}
                    label={this.state.dialog.label}
                >{this.state.dialog.message}</Notifications>

                <div className={styles.login_wrap}>
                    <div className={styles.logo}></div>
                    <TextField
                        id = "login"
                        floatingLabelStyle = {styles.label}
                        inputStyle = {styles.inputStyle}
                        underlineFocusStyle = {styles.underlineFocusStyle}
                        fullWidth = {true}
                        floatingLabelText="Логин или E-Mail"
                        disabled = {this.state.disabled}
                    />
                    <TextField
                        id = "password"
                        type= "password"
                        floatingLabelStyle = {styles.label}
                        inputStyle = {styles.inputStyle}
                        underlineFocusStyle = {styles.underlineFocusStyle}
                        fullWidth = {true}
                        floatingLabelText="Пароль"

                        disabled = {this.state.disabled}
                    />

                    <div className={styles.progress} >
                        <CircularProgress
                            color="white"
                            style={{display:this.state.progress.display}}
                        />
                    </div>

                    <RaisedButton
                        onTouchTap={this.submit.bind(this)}
                        label="Войти" backgroundColor = {styles.submit.backgroundColor}
                        labelColor = {styles.submit.labelColor} style = {styles.submit}
                        disabled = {this.state.disabled}
                    />
                </div>

            </div>
        );
    }

}

export default Login;