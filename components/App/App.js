import  React from 'react';
import  ReactDOM from 'react-dom';
import  Login from 'components/Login';
import  Main from 'components/Main';
import  styles from './App.scss'
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

var Component = React.createClass({


    componentWillMount: function(){
        this.setState({
            component: <Login />
        });
    },

    render: function(){
        return (
            this.state.component
        );
    }
});
var app = document.createElement('div');
app.id = 'app';
document.body.appendChild(app);
ReactDOM.render(<Component />, app);

